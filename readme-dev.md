# SC2 flasher (development) 
[◄ back to readme](readme.md)

[TOC]

## Flash a SC2 unit
To flash a virgin unit is required to connect it to the same network as your PC an turn it on. As it's uboot environment is the default, it automatically will connect via tftp and nfs.

For units that have been flashed before is required the default environment. Depending on the scenario, you could do:
1. If the PC have IP 198.168.1.199 and serial port is available, turn on the device and while uboot is loading paste the password
   ```
	https://quercus.atlassian.net/wiki/spaces/SCIN/pages/1138229249
   ```
    After that, execute 
   ```
	env default -a; boot
   ```
   
2. If the PC have IP **different** than 198.168.1.199 and serial port is available, turn on the device and while uboot is loading paste the password
   ```
	https://quercus.atlassian.net/wiki/spaces/SCIN/pages/1138229249
   ```
   After that, execute 
   ```
	env default -a; setenv nfslan 'setenv autoload no; dhcp; setenv serverip {your.ip}'; boot
   ```

3. If there is no serial port available, open a terminal via ssh and execute
   ```
	FlashRW && fw_setenv bootcmd "env default -a; setenv nfslan 'setenv autoload no; dhcp; setenv serverip {your.ip}'; boot" && Reboot
   ```

4. You can confirm that the unit is being flased observing the led color patterns:
   1. If Id mode: turquoise purple yellow.
   2. If Guidance mode: turquoise purple orange.
   3. Once finished the led will blink a green light.
   4. If an error ocurred the led will blink a red light.


### Re-Flash SC2 unit mantaining its SN and Batch number
In order to flash a unit and mantain its SN & batch number, is required to add as variable of uboot: *qremake=yes*. This can be done via serial port or via ssh:

1. If the unit is connected via serial port, you should follow the instructions given before and add the variable after the order *env default -a*. For instance:

   Turn on the device and while uboot is loading paste the password
   ```
	https://quercus.atlassian.net/wiki/spaces/SCIN/pages/1138229249
   ```
   After that, execute 
   ```
	env default -a; setenv qremake yes; boot
   ```
2. If the unit is connected via ssh, you should follow the instructions and add the variable before *Reboot*.
   ```
	FlashRW && fw_setenv bootcmd "env default -a; setenv nfslan 'setenv autoload no; dhcp; setenv serverip {your.ip}'; boot" && fw_setenv qremake "yes" && Reboot
   ```

## Development Website URL
https://stageapps.quercus-technologies.com/