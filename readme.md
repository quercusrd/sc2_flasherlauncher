# SC2 flasher
[TOC]

Tools to flash the firmware to the SC2 units as well as to lauch the QC system.

## Getting Started

Included tools:

-  `launchSC2Flasher.sh`: flasher launcher.
-  `launchDhcp.sh`: DHCP server launcher (just for 2 ethernes scenarios).
-  `launchSC2QC.sh`: QC launcher.
-  `update.sh`: updates the system to the last version.

### Prerequisites

1. It's required that the PC where the flasher will be launched has two network interfaces:
    - Private interface: This network will connect the PC and the SC Indoor units. The IP of this network have to be **192.168.1.199/255.255.255.0**.
    - Private public: This network needs to have internet connection.
    - For development environments, please refer the [readme-dev](readme-dev.md).
2. Docker must be installed.

```
sudo apt-get install docker
```

3. nfs-kernel-server must be installed.
```
sudo apt-get install nfs-kernel-server
```

## Tools

### Website interface

- Open URL https://prodapps.quercus-technologies.com and sign in with your Quercus credentials. Select section "Spot Control Indoor"

- **Configuration**: There are three variables:
    - **Batch**: Is the current batch number. The batch number can be increased pressing button **Action**.
    - **Serial Number**: Is the next serial number that will be assigned.
    - **SC2 API version**: API version of the production server.

- **Production** and **QC**:  To filter productions or qc there are two options:
    - Show all productions/qcs : Just click on the search button
    - Filter by field: Select the field to filter.



### SC2Flasher

Run the SC2 flasher:

```
./launchSC2Flasher.sh --production
```

For further information about the app:
```
./launchSC2Flasher.sh --help
```

### DHCP server


The dhcp launcher launches a dhcp server.
This app must only be executed when flasher system is running over 2 ethernets (public: internet; private: units).

The dhcp server must run over an ethernet where mask=255.255.255.0 and ip=192.168.1.199.
```
./launchDhcp.sh
```

For further information about the app:
```
./launchDhcp.sh --help
```
### Production QC

Run the SC2 QC:
```
./launchSC2QC.sh --production
```

For further information about the app:
```
./launchSC2QC.sh --help
```

### System Update

Use this tool to update the system to the last version.

```
./update.sh
```

## Common issues

### launchSC2Flasher / Docker logout

If docker login error: “Error response from daemon: pull access denied for quercustechnologies/sc2-flasher, repository does not exist or may require 'docker login': denied: requested access to the resource is denied”

- Create a dockerhub account.
- Contact with the IT Dpt to be included to the quercustechnologies organization and to have acces permissions to the sc2 flasher repository.
- Login with your dockerhub account
```
docker login
```

### launchSC2Flasher / Tftp can't start

Sometimes tftpd-hpa can't execute, so launchSC2Flasher is automatically closed once executed (no dialog pops-up).

When this happens, check if tftpd-hpa service is already running on your system:
```
sudo service tftpd-hpa status
sudo service tftpd-hpa stop
```

If it was not running, just install it  and remove it (it sounds stupid... but it works.)
```
sudo apt-get install tftpd-hpa
sudo apt-get remove tftpd-hpa
```

### launchSC2Flasher / Port 111 and 2049 in use

The SpotControl2 is flashed via NFS v2. This protocol requires the usage of ports **111** and **2049**. For that reason, the host PC have to have those ports free.

#### launchSC2Flasher / Port 111 in use

The host PC have to have the service rpcbind disabled. To check that the port is in use.

```
ss -tpna | grep 111
```

If a line is printed, the port is in use. If this happens, disable the service executing:

```
sudo systemctl stop rpcbind
sudo systemctl disable rpcbind
sudo systemctl mask rpcbind
sudo systemctl stop rpcbind.socket
sudo systemctl disable rpcbind.socket
```

This service will remain masked (disbled) and port 111 will be free even after a reboot. This step should be done on the installation of the production PC.

To revert this change execute:

```
sudo systemctl unmask rpcbind
sudo systemctl enable rpcbind
sudo systemctl start rpcbind
```

#### launchSC2Flasher / Port 2049 in use

The port 2049 have not to be in use. To check that the port is in use execute.

```
ss -tpna | grep 2049
```

To set the port free is required to start and stop the nfs-kernel-server

```
sudo pkill rpcbind
sudo service nfs-kernel-server start
sudo service nfs-kernel-server stop
sudo pkill rpcbind
```

### launchSC2Flasher / Nginx can't start

#### launchSC2Flasher / Port 123 in use (NTP)

Check that port udp 123 is not in use:

```
ss -upna | grep 123
```

Usually it's used by a ntp client running on the host PC. If so, stop it by running:


```
sudo service ntp stop
```

#### launchSC2Flasher / Nginx (others)

Check that port tcp 8080 is not in use or Nginx:

```
ss -tpna | grep 8080
```

Another consequence could be that nginx is running on the host PC. Stop it by running:

```
sudo service nginx stop
```

### launchSC2Flasher / NFS is not working on the server even no errors detected by launchSC2Flasher

Check if NFS service is running OK:

```
systemctl status all nfs-server.service
```

If the following error appears:
```
rpc.nfsd: unable to set any sockets for nfsd
```
Run the following instructions:
 
* 1. sudo rpcbind
* 2. systemctl start nfs-server.service
* 3. kill rpcbind process
* 4. start launchSC2Flasher

Then embedded kernel can mount the NFS server filesystem.

## Documentation

* Analysis: [google-drive](https://docs.google.com/document/d/1ST7n5HK6UzfmlZg4wywIPHQEEdvI6vW4EdqJJQMYmb8/edit?usp=sharing)
* Developers tips: [readme-dev](readme-dev.md)

## Authors

* **Quercus Technologies - R&D**
