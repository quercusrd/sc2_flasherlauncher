# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Develop] -
### Changed
- SCIV2-1605: Readable uBoot password removed from readme flasher


## [2] -
### Added
- SCIV2-490: index.html replaced by the QuercusApp website.
- SCIV2-497: readme.md now describes how to workaround NTP service collisions.


## [1] -
### Added
- First Flasher Launcher version.
