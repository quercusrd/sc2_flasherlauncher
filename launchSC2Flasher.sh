#!/bin/bash
set -euo pipefail

#---------------------------------------------------------------------
# (c) Quercus Technologies (jul'2021)
#---------------------------------------------------------------------
#Descr: Runs the SC2 flasher or QC docker images
#---------------------------------------------------------------------
IsModuleLoaded_()
{
        lsmod | grep "$1" > /dev/null    2>&1
        return $?
}

IsServiceLoaded_()
{
        service --status-all | grep "$1" > /dev/null    2>&1
        return $?
}


Exit_()
{
        echo -e "$1"
        exit 1
}

#---------------------------------------
# V A R I A B L E S    &    C H E C K S
#---------------------------------------
currentPath="$(pwd)/$(dirname $0)"
docPath="$currentPath/doc"
localPath="$currentPath/local"
logsPath="$currentPath/logs"

qc=0
[ ! -z "$(echo $(basename $0) | grep -i qc)" ] && qc=1
versionsFile="./Versions.cfg"
imageName="quercustechnologies/sc2-flasher"
[ $qc -eq 1  ] && imageName=quercustechnologies/sc2-productionqc
defaultImageVersion="latest"
mode="--production"
imageVersion=""
pull=true

[ $qc -eq 0  ] && modules=( "nfs" "nfsd" ) && services=( "nfs-kernel-server" )

while [ $# -gt 0 ]; do
	if [ "$1" == "--develop" ]; then
		mode=$1
		shift
	elif [ "$1" == "--production" ]; then
		mode=$1
		shift
	elif [ "$1" == "--local" ]; then
		pull=false
		shift
	elif [ -z "$imageVersion" ] && [ "$1" != "--help" ]; then
		imageVersion=$1
		shift
	else
		[ "$1" != "--help" ] && echo "Invalid parameters"
		echo -e "$(basename $0): [OPTIONS] [image version]"
		echo -e "\t--develop: develop mode. Generate a random serial number."
		echo -e "\t--production: production mode (default)."
		echo -e "\t--local: does not try to pull the image (develop purposes)"
		echo -e "\t- default image version: $defaultImageVersion" 
		[ "$1" != "--help" ] && exit 1
		exit 0
	fi
done

[ -z "$imageVersion" ] && imageVersion="$defaultImageVersion"

image="$imageName:$imageVersion"

#---------------------------------------
# M A I N
#---------------------------------------
[ -z $(which docker) ] &&
        Exit_ "Docker is not installed!\nTry running: apt install docker"       

for module in ${modules[@]}; do
	IsModuleLoaded_ "$module" ||
		Exit_ "Module $module not loaded.\nTry running: modprobe $module"
done

for service in ${services[@]}; do
	IsServiceLoaded_ "$service" ||
		Exit_ "Service $service not loaded.\nTry running: apt install $service"
done

if $pull; then
    docker pull "$image"
fi

if [ $qc -eq 0 ]; then	
	echo "NFS server Start-Stop service."
	sudo rpcbind
	sudo service nfs-kernel-server start
	sudo service nfs-kernel-server stop
	sudo pkill rpcbind
	
	docker run -it --rm --net=host --privileged "$image" $mode

else
	xhost local:docker && docker run -it --rm --net=host --privileged -v /tmp/.X11-unix:/tmp/.X11-unix \
	-e DISPLAY=unix$DISPLAY "$image" $mode
fi