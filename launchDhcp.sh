#!/bin/bash
#------------------------------------------------------------------
# Quercus Technologies (jul'21)
#------------------------------------------------------------------

Error_()
{
	echo -e $R"Error: $1"$D
	exit 1
}

Usage_()
{
	[ $1 -gt 0 ] && echo -e $R"Invalid params."$D
	echo -e "\e[1m$(basename $0)\e[0m: it runs a dhcp server over <eth>"
	echo -e "  dhpcd.conf and leases files written on $tmp"
	echo -e "  dhcp must run over an eth where mask=255.255.255.0 and ip=192.168.1.199"
	echo -e "\e[1mUsage: $(basename $0) <eth> <--help>\e[0m"
	echo -e "\t--help: prints usage text."
	echo -e "\t<eth> : ethernet interface (device)."
	echo -e "\t        If no option, pick a device from the dialog block."
	echo -e "\t        Execute \"nmcli device\" to list all devices available."
	exit $1
}

EthSelector_()
{
	tmpDir=$(mktemp -d /tmp/XXXXX)
	unset lans
	options=""
	for eth_int in $(/sbin/ifconfig | grep -v '^ ' | cut -d ':' -f1 -s | egrep -v '^lo$|^docker'); do
		ip=$(ip addr show $eth_int | grep -o "inet [0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | head -1 )
		if [ "$ip" == "192.168.1.199" ]; then
			eth=$eth_int
			break		
		fi
	done

	[ "$eth" == "" ] && Error_ "No network interface detected with IP = 192.168.1.199."	
}


#------------------------------------------------------------------
# M A I N 
#------------------------------------------------------------------
tmp="/tmp/$(basename $0)"
R="\e[31m"
G="\e[32m"
D="\e[39m"


#-------------------
# Get params
#-------------------
eth=""
while [ $# -gt 0 ]; do
	if [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
		Usage_ 0
	elif [ -z $eth ]; then
		eth=$1
	else
		EthSelector_
	fi
	shift
done

#------------------- 
# Check params
#-------------------
[ -z "$eth" ] && EthSelector_

echo "Using as ethernet interface $eth."

ip=$( ip addr show $eth 2>/dev/null | grep -o "inet [0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" )
[ $? -ne 0 ] && Error_ "Device $eth does not exist."
brd=$(ip addr show $eth 2>/dev/null | grep -o "brd [0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" )
ipHigh=$(echo $ip | cut -d '.' -f1-3)
ipLow=$(echo $ip | cut -d '.' -f4 | cut -d ' ' -f1)
brdHigh=$(echo $brd | cut -d '.' -f1-3)
brdLow=$(echo $brd | cut -d '.' -f4 | cut -d ' ' -f1)


[ $ipLow -ne 199 ]   && Error_ "$eth invalid ip   (current: $ip | target: $ipHigh.199)"
[ $brdLow -ne 255 ] && Error_ "$eth invalid mask (target: 255.255.255.0)"
[ ! -z $(echo $brdHigh | grep 255) ] && Error_ "$eth invalid mask (target: 255.255.255.0)"

#-------------------
# Run
#-------------------

ipFirst=$ipHigh.10
ipLast=$ipHigh.254
netmask=255.255.255.0

#[ -d "$tmp" ] && rm $tmp -r
mkdir -p $tmp
cp .dhcpd/dhcpd.conf $tmp/

# This is a very basic subnet declaration.
sed -i "s/@routers@/$ip/g"      $tmp/dhcpd.conf
sed -i "s/@subnet@/$ipHigh.0/g" $tmp/dhcpd.conf
sed -i "s/@netmask@/$netmask/g" $tmp/dhcpd.conf
sed -i "s/@firstip@/$ipFirst/g" $tmp/dhcpd.conf
sed -i "s/@lastip@/$ipLast/g"   $tmp/dhcpd.conf



echo -e $G"Configuration is correct. Launching docker image..."$D
docker run -it --rm --init --net host -v "$tmp":/data networkboot/dhcpd $eth
